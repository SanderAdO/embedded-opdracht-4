#include "ud_ucase.h"
#include <wiringPi.h>

int main(int argc, char *argv[])
{

    wiringPiSetup();

    struct sockaddr_un svaddr, claddr;
    int sfd, j;
    ssize_t numBytes;
    socklen_t len;
    char buf[BUF_SIZE];

    sfd = socket(AF_UNIX, SOCK_DGRAM, 0);       /* Create server socket */
    if (sfd == -1)
        errExit("socket");

    /* Construct well-known address and bind server socket to it */
    if (strlen(SV_SOCK_PATH) > sizeof(svaddr.sun_path) - 1)
        fatal("Server socket path too long: %s", SV_SOCK_PATH);

    if (remove(SV_SOCK_PATH) == -1 && errno != ENOENT)
        errExit("remove-%s", SV_SOCK_PATH);

    memset(&svaddr, 0, sizeof(struct sockaddr_un));
    svaddr.sun_family = AF_UNIX;
    strncpy(svaddr.sun_path, SV_SOCK_PATH, sizeof(svaddr.sun_path) - 1);

    if (bind(sfd, (struct sockaddr *) &svaddr, sizeof(struct sockaddr_un)) == -1)
        errExit("bind");

    t_data received_data;

    len = sizeof(struct sockaddr_un);

    numBytes = recvfrom(sfd, &received_data, sizeof(received_data), 0,
                            (struct sockaddr *) &claddr, &len);
    if (numBytes == -1)
        errExit("recvfrom");

    /*
    if (sendto(sfd, &received_data, sizeof(received_data), 0, (struct sockaddr *)&claddr, len) != numBytes)
        fatal("sendto");
    */

    for (;;) {

        pinMode(received_data.IO, OUTPUT);
        digitalWrite (received_data.IO, HIGH) ;
        delay(received_data.period);
        received_data.state = 1;
        if (sendto(sfd, &received_data, sizeof(received_data), 0, (struct sockaddr *)&claddr, len) != numBytes)
            fatal("sendto");        
        digitalWrite (received_data.IO, LOW) ;
        delay(received_data.period);
        received_data.state = 0;
        if (sendto(sfd, &received_data, sizeof(received_data), 0, (struct sockaddr *)&claddr, len) != numBytes)
            fatal("sendto");    
    }
}
