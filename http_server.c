#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <netdb.h> // for getnameinfo()

// Usual socket headers
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <arpa/inet.h>

#include <wiringPi.h>

#define SIZE 1024
#define BACKLOG 10  // Passed to listen()
#define MAXLINE 1000
#define PORT 8001
#define LENGTH 8000

void report(struct sockaddr_in *serverAddress);
void addGpioData();

void error(const char *msg) {
 perror(msg);
 exit(1);
}

void setHttpHeader(char httpHeader[])
{
    memset(httpHeader, 0, LENGTH);

    addGpioData();

    // File object to return
    FILE *htmlData = fopen("../index.html", "r");

    if(htmlData == NULL)
    {
        /* Unable to open file */
        printf("Unable to open file.\n");
        printf("Please check whether file exists and you have read privileges.\n");
    }

    char line[MAXLINE];
    char responseData[LENGTH];
    while (fgets(line, MAXLINE, htmlData) != 0) {
        strcat(responseData, line);
    }
    // char httpHeader[LENGTH] = "HTTP/1.1 200 OK\r\n\n";
    strcat(httpHeader, "HTTP/1.1 200 OK\r\n\n");
    strcat(httpHeader, responseData);

    fclose(htmlData);
}

void addGpioData()
{
    wiringPiSetupGpio();

    FILE *htmlData;
    int state, mode;
    char* ALT_MODES[8] = {"input", "output", "alt5", "alt4", "alt0", "alt1", "alt2", "alt3"};

    htmlData = fopen("../index.html", "w+");

    if(htmlData == NULL)
    {
        /* Unable to open file */
        printf("Unable to open file.\n");
        printf("Please check whether file exists and you have read privileges.\n");
    }
    fseek(htmlData, 0, SEEK_END);
    //add header with bootstrap css
    fputs("<html>\n\t<head>\n\t\t<link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU\" crossorigin=\"anonymous\">\n\t\t", htmlData);
    fputs("<title>Embedded Opdracht 4</title>\n\t\t<h1>Embedded Systems Opdracht 4: Sockets</h1>\n\t\t<h5>State of GPIO's</h5>\n\t</head>\n\t<body>\n\t\t", htmlData);
    //add table with column headers
    fputs("<table class=\"table table-striped table-dark w-auto\">\n\t\t\t<tr>\n\t\t\t<th scope=\"col\">BCM</th>\n\t\t\t<th scope=\"col\">Mode</th>\n\t\t\t<th scope=\"col\">State</th>\n\t\t\t</tr>\n", htmlData);

    // add row for each GPIO (GPIO nums are BCM):
    for(int i=0; i<28; i++){
        state = digitalRead(i);
        mode = getAlt(i);

        char gpio_data[100];

        sprintf(
            gpio_data,
            "\t\t\t<tr>\n\t\t\t<td>%d</td>\n\t\t\t<td>%s</td>\n\t\t\t<td>%d</td>\n\t\t\t</tr>\n",
            i, ALT_MODES[mode], state);
        fputs(gpio_data, htmlData);
    }
    //close table and body + add bootstrap javascript
    fputs("\t\t</table>\n\t\t<script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js\" integrity=\"sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ\" crossorigin=\"anonymous\"></script>\n\t</body>\n</html>\n", htmlData);
  
    fclose(htmlData);
}

int main(void)
{
    char httpHeader[LENGTH] = "HTTP/1.1 200 OK\r\n\n";
    int pid;

    // Socket setup: creates an endpoint for communication, returns a descriptor
    // -----------------------------------------------------------------------------------------------------------------
    int serverSocket = socket(
        AF_INET,      // Domain: specifies protocol family
        SOCK_STREAM,  // Type: specifies communication semantics
        0             // Protocol: 0 because there is a single protocol for the specified family
    );

    // Construct local address structure
    // -----------------------------------------------------------------------------------------------------------------
    struct sockaddr_in serverAddress;
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(PORT);
    serverAddress.sin_addr.s_addr = htonl(INADDR_LOOPBACK);//inet_addr("127.0.0.1");

    // Bind socket to local address
    // -----------------------------------------------------------------------------------------------------------------
    // bind() assigns the address specified by serverAddress to the socket
    // referred to by the file descriptor serverSocket.
    bind(
        serverSocket,                         // file descriptor referring to a socket
        (struct sockaddr *) &serverAddress,   // Address to be assigned to the socket
        sizeof(serverAddress)                 // Size (bytes) of the address structure
    );

    // Mark socket to listen for incoming connections
    // -----------------------------------------------------------------------------------------------------------------
    int listening = listen(serverSocket, BACKLOG);
    if (listening < 0) {
        error("Error: The server is not listening.\n");
    }

    struct sockaddr_in clientAddress;
    socklen_t clilen;

    clilen = sizeof(clientAddress);

    report(&serverAddress);     // Custom report function
    //setHttpHeader(httpHeader);  // Custom function to set header
    int clientSocket;

    // Wait for a connection, create a connected socket if a connection is pending
    // -----------------------------------------------------------------------------------------------------------------
    while(1) {
        clientSocket = accept(serverSocket, (struct sockaddr *) &clientAddress, &clilen);

        if(clientSocket < 0){
            error("Error on accept");
        }

        pid = fork();
        if (pid < 0)
             error("Error on fork");
        if (pid == 0)  {
            close(serverSocket);
            setHttpHeader(httpHeader);
            send(clientSocket, httpHeader, sizeof(httpHeader), 0);
            exit(0);
        }
        else close(clientSocket);
    }
        close(serverSocket);
        return 0;
}

void report(struct sockaddr_in *serverAddress)
{
    char hostBuffer[INET6_ADDRSTRLEN];
    char serviceBuffer[NI_MAXSERV]; // defined in `<netdb.h>`
    socklen_t addr_len = sizeof(*serverAddress);
    int err = getnameinfo(
        (struct sockaddr *) serverAddress,
        addr_len,
        hostBuffer,
        sizeof(hostBuffer),
        serviceBuffer,
        sizeof(serviceBuffer),
        NI_NUMERICHOST
    );

    if (err != 0) {
        error("It's not working!!\n");
    }
    printf("\n\n\tServer listening on http://%s:%s\n", hostBuffer, serviceBuffer);
}